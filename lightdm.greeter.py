#!/usr/bin/python
#
# Author: Matt Fischer <matthew.fischer@canonical.com>
# Copyright (C) 2012 Canonical, Ltd
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version. See http://www.gnu.org/copyleft/gpl.html the full text of the
# license.
#
# This code is based on the LightDM GTK Greeter which was written by:
# Robert Ancell <robert.ancell@canonical.com>

from gi.repository import GObject
from gi.repository import GLib
from gi.repository import Gtk
from gi.repository import LightDM
from gi.repository import Gdk
import sys

greeter = None
background_window = None
login_window = None

def onLoginButton(widget):
    greeter.authenticate_as_guest()

handlers = {
    "on_loginButton_clicked": onLoginButton
}

def authentication_complete_cb(greeter):
    greeter.start_session_sync("Lubuntu")

if __name__ == '__main__':
    main_loop = GObject.MainLoop()
    builder = Gtk.Builder()
    greeter = LightDM.Greeter()

    # connect builder and widgets
    builder.add_from_file("/opt/quiosc/lightdm/quiosc-greeter.ui")

    cursor = Gdk.Cursor.new(Gdk.CursorType.ARROW)
    background_window = builder.get_object("backgroundWindow")
    background_window.get_root_window().set_cursor(cursor)
    background_window.show()

    login_window = builder.get_object("loginWindow")

    # connect signals to Gtk UI
    builder.connect_signals(handlers)

    # connect signal handlers to LightDM & connect to greeter
    greeter.connect("authentication-complete", authentication_complete_cb)
    greeter.connect_sync()

    # setup the GUI
    login_window.show()

    main_loop.run ()
