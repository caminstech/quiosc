#!/bin/bash -e

sudo apt-get install wget

sudo mkdir -p /opt/quiosc/shutdown

sudo cp shutdown.script.sh /opt/quiosc/shutdown/shutdown.sh
sudo chmod a+x /opt/quiosc/shutdown/shutdown.sh

# Si executo una vegada no funciona !?
line="*/5 * * * * /opt/quiosc/shutdown/shutdown.sh"
(sudo crontab -u root -l; echo "$line") | sort - | uniq - | sudo crontab -u root -
(sudo crontab -u root -l; echo "$line") | sort - | uniq - | sudo crontab -u root -
