#!/bin/bash -e

status=$(wget http://canal.camins.upc.edu/index.php/public/estat?terminal=Q1 -q -O -)
if [ "$status" != "actiu" ]; then
	/sbin/shutdown -h now
fi
