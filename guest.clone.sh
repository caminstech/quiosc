#!/bin/bash -e

FOLDER=$1

if [ "$FOLDER" == "" ]; then
	echo "Cal indicar el directori origen"
	exit -1
fi;

rm -rf guest.skel
mkdir guest.skel
sudo cp -rT $1 guest.skel
sudo chown -R $USER:$USER guest.skel
