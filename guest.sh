#!/bin/bash -e

sudo apt-get install firefox libreoffice
sudo rm -rf /etc/guest-session
sudo mkdir /etc/guest-session
sudo cp -rT guest.skel /etc/guest-session/skel

PREFS=/etc/guest-session/skel/.mozilla/firefox/6eqfwvw0.default/prefs.js
sudo sed -i -e 's/^user_pref("extensions.dlwatch.pass", "");$/user_pref("extensions.dlwatch.pass", "*");/' $PREFS
sudo sed -i -e 's/^user_pref("extensions.procon.general.password", "");$/user_pref("extensions.procon.general.password", "*");/' $PREFS
