#!/bin/bash -e

SYSTEM=$(lsb_release -sir | tr '\n' ' ' | sed 's/ *$//g')
if [ "$SYSTEM" != "Ubuntu 12.04" ]; then
	echo "El sistema '$SYSTEM' no està suportat."
	exit
fi;

sudo apt-get install liblightdm-gobject-1-0 gir1.2-lightdm-1 python-gobject gir1.2-glib-2.0 gir1.2-gtk-3.0

sudo mkdir -p /opt/quiosc/lightdm
sudo cp lightdm.*.png /opt/quiosc/lightdm

sudo cp lightdm.conf /etc/lightdm/lightdm.conf
sudo cp lightdm.greeter.conf /etc/lightdm/lightdm-gtk-greeter.conf
sudo cp lightdm.greeter.desktop /usr/share/xgreeters/quiosc-greeter.desktop
sudo cp lightdm.greeter.py /opt/quiosc/lightdm/quiosc-greeter.py
sudo cp lightdm.greeter.ui /opt/quiosc/lightdm/quiosc-greeter.ui
sudo chmod a+x /opt/quiosc/lightdm/quiosc-greeter.py

sudo sed -i "s/UID_MIN[ |\t].*$/UID_MIN 9999/g" /etc/login.defs

sudo mv /usr/share/xsessions/Lubuntu.desktop /tmp/Lubuntu.desktop
sudo rm -f /usr/share/xsessions/*.desktop
sudo mv /tmp/Lubuntu.desktop /usr/share/xsessions/Lubuntu.desktop

sudo rm -f /usr/share/lubuntu/wallpapers/lubuntu-default-wallpaper.png
sudo ln -s /opt/quiosc/lightdm/lightdm.login.png /usr/share/lubuntu/wallpapers/lubuntu-default-wallpaper.png

sudo mkdir -p /etc/polkit-1/localauthority/50-local.d/
sudo cp lightdm.disable.shutdown.pkla /var/lib/polkit-1/localauthority/50-local.d/disable-shutdown.pkla

sudo cp lightdm.blankscreen.conf /usr/share/X11/xorg.conf.d/00-blankscreen.conf
