#!/bin/bash -e

# Llibreries QT
sudo apt-get install python-qt4 python-qt4-dev

sudo mkdir -p /opt/quiosc/session

sudo cp session.logout.sh /opt/quiosc/session/logout.sh
sudo chmod a+x /opt/quiosc/session/logout.sh

sudo cp session.timer.py /opt/quiosc/session/timer.py
sudo chmod a+x /opt/quiosc/session/timer.py
sudo mkdir -p /etc/guest-session/skel/.config/autostart
sudo cp session.timer.desktop /etc/guest-session/skel/.config/autostart/timer.desktop
