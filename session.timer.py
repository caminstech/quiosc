#!/usr/bin/python
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4.QtGui import QSpacerItem
from PyQt4.QtCore import pyqtSlot,SIGNAL,SLOT
from PyQt4 import Qt
import threading
import sys
import os
import gtk

class Window(QtGui.QWidget):
  def __init__(self):
    self.minutes = 8
    self.seconds = 0

    self.setWindow()
    self.button = self.logoutButton()
    self.label = self.logoutCountdown()

    layout = QtGui.QGridLayout(self)
    layout.addWidget(self.button,0,0)
    layout.addItem(QtGui.QSpacerItem(40,4),0,1)
    layout.addWidget(self.label,0,2)

    self.countdown()

  def setWindow(self):
    QtGui.QWidget.__init__(self, None, QtCore.Qt.ToolTip)
    screen = gtk.Window().get_screen()
    self.setGeometry((screen.get_width()-200)/2, 0, 200, 40)
    self.setStyleSheet("color:white;background-color:#9e1a32;");

  def logoutButton(self):
    button = QtGui.QPushButton("Close session", self)
    button.setStyleSheet("padding:0.2em;color:#333;background-color:white");
    button.clicked.connect(self.handleButton)
    return button

  def logoutCountdown(self):
    label = Qt.QLabel("")
    label.setStyleSheet("font-weight:bold;padding:0.2em;");
    return label

  def handleButton(self):
    self.logout()

  def updateCountdown(self):
      self.label.setText("Time left: " + str(self.minutes) + ":" + str(self.seconds).zfill(2))

  def countdown(self):
    self.removeOneSecond()
    if self.seconds <= 0 and self.minutes <= 0 :
      self.logout()
    else :
      self.updateCountdown()

    threading.Timer(1.0, self.countdown).start()

  def removeOneSecond(self):
    if self.seconds > 0 :
      self.seconds = self.seconds - 1
    else :
      if self.minutes > 0 :
        self.minutes = self.minutes - 1
        self.seconds = 59

  def logout(self):
    os.execl("/opt/quiosc/session/logout.sh", "logout")

def main(argv):
  app = QtGui.QApplication(sys.argv)
  window = Window()
  window.show()
  sys.exit(app.exec_())

main(sys.argv)

